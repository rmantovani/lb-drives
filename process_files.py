
#initialization of libraries
### %matplotlib inline
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import os
import glob
import itertools

#setup variables
input_file = "C:\\Users\\dedus-robertoma\\Dropbox\\Langerbrugge drives\drives_input.xlsx"
output_file = "C:\\Users\\dedus-robertoma\\Dropbox\\Langerbrugge drives\drives_output.xlsx"
#input_file_tst = "C:\\Users\\dedus-robertoma\\Dropbox\\Langerbrugge drives\drives_output_tst.xlsx"
path = "C:\\Users\\dedus-robertoma\\Dropbox\\Langerbrugge drives"

os.chdir(path)

drives = ["LM-41BAL380.drv", "LM-41SHL009.2.drv", "LM-41BAL300.drv", "LM-41BFL060.1.drv", 
          "LM-41BFL030.1.drv", "LM-41SHL015.drv", "LM-55BRL041.drv", "LM-34CHP102.drv",
          "LM-41SHL009.1.drv", "LM-34LBL102.1.drv" ]     

            
#%%
#load files into pandas
df_output = pd.read_excel(output_file)
df_input = pd.read_excel(input_file)

#%%
#print df dimensions
input_columns = df_input.columns
drives = set(map(lambda x: re.search('LM.+\.drv',x).group(0), input_columns))
print("\nInput df->")
print("Number of columns:",len(df_input.columns))
print("Number of drives:",len(drives))
print("Number of variables/drive:",len(df_input.columns)/len(drives))
output_columns = df_output.columns
drives = len(output_columns)
print("\nOutput df->")
print("Number of columns:",len(output_columns))
print("Number of drives:", drives)
print("Number of variables/drive:",len(output_columns)/drives)

#%%
#cleaning columns
df_input = df_input.rename(columns=lambda x: re.search('LM.+\.drv.+',x).group(0))
df_input = df_input.rename(columns=lambda x: re.sub('\|','_',x))
df_input = df_input.rename(columns=lambda x: re.sub(' ','_',x))
df_input.columns

            
#%%
#compute changes in status
columns = df_output.columns
num_inc={}
for i in columns:
    print(i)
    m=0
    status = 0
    changes = 0
    for n in df_output[i]:
        if n == status:
            pass
        else:
            changes = changes + 1
        status = n
        if n == 1: 
            #print i, n
            m = m + 1
            num_inc[i] = m
    print("changes:", changes)
print(num_inc)

 #%%
#function to split dataframe
def split(df,prefix,type="o"):
    #split drives df into smaller files based on the drive name
    size = len(df.columns)
    work_path = "work_files\\"
    if type == "i":
        col = 7
        for i in range(0, size):
            if i % col == 0:
                df_in = df.ix[:,i:i+col]
                name = re.search('LM.+\.drv',df_in.columns[1]).group(0)
                file = work_path+prefix+name+'.xlsx'
                df_in.to_excel(file)
    else:
        col = 1
        for i in range(0, size):
            df_out = df.ix[:,i:i+col]
            print(df_out.columns[0])
            text = re.search('LM.+\.[a-z0-9]+',df_out.columns[0]).group(0)
            if text.find(".drv") < 0:
                filename = work_path+prefix+text+".drv"+'.xlsx'
                text = text+".drv"+"_status"
            else:
                filename = work_path+prefix+text+'.xlsx'
                text = text+"_status"
            print("name is:",text)
            colname = df_out.columns[0]
            df_out = df_out.rename(columns={colname: text})
            df_out.to_excel(filename)
    return           


#%% 
# splitting files
split(df_input,"input_",'i')
split(df_output,"output_",'o')

